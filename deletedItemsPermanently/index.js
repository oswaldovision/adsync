const {deleteUser, getToken} = require('./../graphHelper')
const {getAllSecretsValues, setSecretsByTenant} = require('./../secretsHelper')

module.exports = async function (context, req) {
  const secretValues = await getAllSecretsValues()
  let settings = setSecretsByTenant(req.body.tenantTo, secretValues)

  const token = await getToken(req.body.tenantTo, settings).then(token => token)

  const options = getOptionsDeletedItem(token, req.body.userId)

  // delete user
  const resultDelete = await deleteUser({
    method: 'DELETE',
    url: 'https://graph.microsoft.com/beta/users/' + req.body.userId,
    headers:
      {
        'Authorization': token,
        'Content-Type': 'application/json'
      }
  })

  //delete user permanently
  const result = await deleteUser(options)

  if (!result) {
    context.res = {
      status: 202,
      body: 'Deleted permanently !'
    }

  } else {
    let objResult = JSON.parse(result)

    if (objResult && objResult.hasOwnProperty('error')) {
      context.res = {
        status: 500,
        body: objResult
      }

    }
  }
}

const getOptionsDeletedItem = (token, userId) => {
  return {
    method: 'DELETE',
    url: 'https://graph.microsoft.com/beta/directory/deleteditems/' + userId,
    headers:
      {
        'Authorization': token,
        'Accept': 'application/json'
      }
  }
}