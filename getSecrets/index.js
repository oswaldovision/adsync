const {getKeyVaultCredentials, getKeyVaultSecret, getAllSecretsValues} = require('./../secretsHelper')

module.exports = async function (context, req) {
  context.log('JavaScript HTTP trigger function processed a request.')

  let allValues = await getAllSecretsValues()

  context.res = {
    body: `Your secret value is: ${JSON.stringify(allValues)}.`
  }

  // let key = req.query.key
  // context.log(`this is a key of value: ${key}`)
  //
  // let credentials = await getKeyVaultCredentials()
  //
  // context.log(`the credentials: ${JSON.stringify(credentials)}`)
  //
  // const secret = await getKeyVaultSecret(credentials, key)
  //
  // context.res = {
  //   body: `Your secret value is: ${secret.value}.`
  // }

  // await getKeyVaultSecret(credentials, key)
  //   .then(function (secret) {
  //     context.res = {
  //       body: `Your secret value is: ${secret.value}.`
  //     }
  //   }).catch(function (err) {
  //     context.res = {
  //       status: 400,
  //       body: `Error ${err}`
  //     }
  //   })
}
