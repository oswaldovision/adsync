const {getToken, inviteUser, patchUser} = require('./../graphHelper')
const {getAllSecretsValues, setSecretsByTenant} = require('../secretsHelper')

const toBodyPatch = datauser => {
  return {
    givenName: datauser.givenName,
    surname: datauser.surname,
    city: datauser.city,
    country: datauser.country,
    department: datauser.department,
    displayName: datauser.displayName,
    jobTitle: datauser.jobTitle,
    mailNickname: datauser.mailNickname,
    mobilePhone: datauser.mobilePhone,
    officeLocation: datauser.officeLocation,
    // onPremisesImmutableId: datauser.onPremisesImmutableId,
    postalCode: datauser.postalCode,
    // preferredLanguage: datauser.preferredLanguage,
    streetAddress: datauser.streetAddress,
    // userPrincipalName: datauser.userPrincipalName,
    // userType: datauser.userType,
    showInAddressList: datauser.showInAddressList == false ? false : true
  }
}

module.exports = async function (context, req) {

  let allValues = await getAllSecretsValues()

  context.log(allValues.tenants.join(','))

  let tenantId = req.body.tenant
  let contact = req.body.contact

  let settings = setSecretsByTenant(tenantId, allValues)

  let tokenTenant = await
    getToken(tenantId, settings)
      .then(token => {
        return token
      })

  let optionsToInvite = {
    method: 'POST',
    url: 'https://graph.microsoft.com/beta/invitations',
    headers:
      {
        'Authorization': tokenTenant,
        'Content-Type': 'application/json'
      },
    body: {
      'invitedUserDisplayName': contact.displayName,
      'invitedUserEmailAddress': contact.mail,
      'invitedUserMessageInfo': {
        'customizedMessageBody': 'Invitacion a Tennant',
        'messageLanguage': 'string'
      },
      'sendInvitationMessage': false,
      'inviteRedirectUrl': 'https://invitations.microsoft.com/redeem',
      'inviteRedeemUrl': 'https://visionsoftware.com'
    },
    json: true
  }
  let idUser = await
    inviteUser(optionsToInvite)
      .then(user => {
        return user.invitedUser.id
      })

  let optionsToPatch = {
    method: 'PATCH',
    url: 'https://graph.microsoft.com/beta/users/' + idUser,
    headers:
      {
        'Authorization': tokenTenant,
        'Content-Type': 'application/json'
      },
    body: toBodyPatch(contact),
    json: true
  }
  await patchUser(optionsToPatch)
    .then(result => {
      return result
    })

  context.res = {
    status: 200,
    body: {idUser},
    headers: {
      'Content-Type': 'application/json'
    }
  }

}
