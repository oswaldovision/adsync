const {getToken, inviteUser, patchUser, getUser} = require('./../graphHelper')
const {getAllSecretsValues, setSecretsByTenant} = require('../secretsHelper')
const {register, existsReprocess} = require('./../process/reprocess')

const toBodyPatch = datauser => {
  return {
    givenName: datauser.givenName,
    surname: datauser.surname,
    city: datauser.city,
    country: datauser.country,
    department: datauser.department,
    displayName: datauser.displayName,
    jobTitle: datauser.jobTitle,
    mailNickname: datauser.mailNickname,
    mobilePhone: datauser.mobilePhone,
    officeLocation: datauser.mobilePhone,
    onPremisesImmutableId: datauser.onPremisesImmutableId,
    postalCode: datauser.postalCode,
    preferredLanguage: datauser.preferredLanguage,
    // state: datauser.state,
    streetAddress: datauser.streetAddress,
    usageLocation: datauser.usageLocation,
    // userPrincipalName: datauser.userPrincipalName,
    // userType: datauser.userType,
    showInAddressList: datauser.showInAddressList === false ? false : true,
    onPremisesExtensionAttributes: {
      extensionAttribute12: datauser.onPremisesExtensionAttributes.extensionAttribute12,
      extensionAttribute13: datauser.onPremisesExtensionAttributes.extensionAttribute13,
      extensionAttribute14: datauser.onPremisesExtensionAttributes.extensionAttribute14

    }
  }
}

const invitationToReprocess = (userNativeId, errorMessage, tenantFrom, tenantTo, optionsToInvite, comments) => {
  const data = {
    functionStack: 'syncNewUser',
    userNativeId,
    attemptEvent: 'inviteUser',
    errorMessage,
    tenantFrom,
    tenantTo,
    body: optionsToInvite,
    comments,
  }
  register(data)
}

const patchToReprocess = (userNativeId, errorMessage, tenantFrom, tenantTo, optionsToInvite, comments) => {
  const data = {
    functionStack: 'syncNewUser',
    userNativeId,
    attemptEvent: 'patchUser',
    errorMessage,
    tenantFrom,
    tenantTo,
    body: optionsToInvite,
    comments,
  }
  register(data)
}

module.exports = async function (context, req) {

  if (!req.query.tenant) {
    context.res = {
      status: 500,
      body: 'El id tenant es requerido !'
    }
    return
  }

  let tenantId = req.query.tenant

  //get tenants from vault
  let allValues = await getAllSecretsValues()

  context.log(allValues.tenants.join(','))

  let tenants = allValues.tenants

  let setTenants = tenants.filter(v => v != tenantId)

  let success = []

  //obtener los datos del user para luego actualizar
  let settings = setSecretsByTenant(tenantId, allValues)
  let result = await getToken(tenantId, settings).then(token => {
    let options = {
      method: 'GET',
      url: 'https://graph.microsoft.com/beta/users/' + req.body.id,
      headers:
        {
          'Authorization': token
        }
    }
    return getUser(options)
  })

  let dataUser = JSON.parse(result)

  let optionsToPatch = {}

//loop de los tenant para crear el usuario de tipo guest
  for (let i = 0; i < setTenants.length; i++) {

    let configValues = setSecretsByTenant(setTenants[i], allValues)
    let tokenTenant = await getToken(setTenants[i], configValues).then(token => {
      return token
    })

    let optionsToInvite = {
      method: 'POST',
      url: 'https://graph.microsoft.com/beta/invitations',
      headers:
        {
          'Authorization': tokenTenant,
          'Content-Type': 'application/json'
        },
      body: req.body,
      json: true
    }

    context.log(`Data to invited user: ${JSON.stringify(req.body)}`)

    await inviteUser(optionsToInvite).then(user => {
      if (user.hasOwnProperty('error')) {
        const message = JSON.stringify(user.error)

        //check if there is previous reprocess
        existsReprocess(dataUser.id, tenantId, setTenants[i], 'inviteUser')
          .then(existsDoc => {
            if (!existsDoc) {
              //Registering Reprocess
              invitationToReprocess(dataUser.id, message, tenantId, setTenants[i], optionsToInvite, `Invitation failed: result error`)
            }
          })

        throw new Error('Invitation failed: result error')
      }

      optionsToPatch = {
        method: 'PATCH',
        url: 'https://graph.microsoft.com/beta/users/' + user.invitedUser.id,
        headers:
          {
            'Authorization': tokenTenant,
            'Content-Type': 'application/json'
          },
        body: toBodyPatch(dataUser),
        json: true
      }

      context.log(`User Invited Success: ${JSON.stringify(result)}`)

      return patchUser(optionsToPatch)
    }).then(user => {
      if (user && user.hasOwnProperty('error')) {
        const message = JSON.stringify(user.error)

        //check if there is previous reprocess
        existsReprocess(dataUser.id, tenantId, setTenants[i], 'patchUser')
          .then(existsDoc => {
            if (!existsDoc) {
              //Registering Reprocess
              patchToReprocess(dataUser.id, message, tenantId, setTenants[i], optionsToPatch, `Patch failed: result error`)
            }
          })

        throw new Error('Patch failed: result error')
      }
      success.push(true)
    }).catch(error => {
      success.push(false)
      context.log(`El Error ! ${error}`)
    })
  }

  if (success.every(value => {return value === true})) {
    context.res = {
      headers: {
        'Content-Type': 'application/json'
      },
      body: {'Sync tenants': setTenants.join(' ')}
    }
  } else {
    context.res = {
      status: 500,
      body: 'Error invited !'
    }
  }

}
