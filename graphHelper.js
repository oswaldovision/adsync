const request = require('request')

const helperGraph = {

  getUser: function (options) {
    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(body)
        }
      })
    })
  },

  getUsers: function (options) {
    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(body)
        }
      })
    })
  },

  deleteUser: function (options) {
    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(body)
        }
      })
    })
  },

  createUser: function (options) {
    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(body)
        }
      })
    })
  },

  inviteUser: function (options) {
    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(body)
        }
      })
    })
  },

  getContacts: function (options) {
    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(body)
        }
      })
    })
  },

  patchUser: function (options) {
    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(body)
        }
      })
    })
  },

  getToken: function (tenantId, secrets) {
    let options = {
      method: 'POST',
      url: `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
      headers:
        {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      form:
        {
          client_id: secrets.clientId,
          scope: secrets.scope,
          client_secret: secrets.clientSecret,
          grant_type: secrets.grantType
        }
    }

    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(JSON.parse(body).access_token)
        }
      })
    })
  }
}

module.exports = helperGraph
