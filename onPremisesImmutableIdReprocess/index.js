const {getonPremisesImmutableIdError, reprocessed, errorReprocess} = require('./../process/reprocess')
const {getUser, getToken, patchUser, inviteUser, deleteUser} = require('./../graphHelper')
const {getAllSecretsValues, setSecretsByTenant} = require('./../secretsHelper')

let secretValues

const reprocessPatchUsers = async (context, docs) => {
  for (let i = 0; i < docs.length; i++) {

    let doc = docs[i]

    const arr = doc.body.url.split('/')
    const userId = arr.slice(arr.length - 1, arr.length)

    const resultDelete = await deleteUser({
      method: 'POST',
      url: 'https://aadsynccore.azurewebsites.net/api/deletedItemsPermanently?code=rkrfeFQBYjaIHlf9Rc0bpBkrl0hVn9ky8FKHR7SqJap8jt17AiaiBA==',
      headers:
        {
          'Content-Type': 'application/json'
        },
      body:
        {
          tenantTo: doc.tenantTo,
          userId: userId[0]
        },
      json: true
    })

    let data

    if (doc.userNativeId) {
      data = await getDataUser(context, doc, doc.userNativeId)
    } else {
      const upn = await getUpn(context, doc, userId[0])
      if (upn) {
        data = await getDataUser(context, doc, upn.split('#EXT#')[0].replace('_', '@'))
      }
    }

    if (data) {
      await updateUser(context, doc, toBodyPatch(data), data)
    }
  }
}

module.exports = async function (context, myTimer) {
  console.log('********************** Start Function Reprocess Errors OnPremisesInmutableId***********************')
  const docsReprocess = await getonPremisesImmutableIdError()

  await reprocessPatchUsers(context, docsReprocess)
  console.log('********************** Finish Function Reprocess Errors OnPremisesInmutableId***********************')
}

const getUpn = async function (context, doc, userId) {
  secretValues = secretValues || await getAllSecretsValues()
  let settings = setSecretsByTenant(doc.tenantTo, secretValues)

  let dataUser = await getToken(doc.tenantTo, settings).then(token => {
    let options = getOptionsUser(token, userId)
    return getUser(options)
  })

  dataUser = JSON.parse(dataUser)

  if (dataUser && dataUser.hasOwnProperty('error')) {
    errorReprocess(
      doc._id,
      dataUser.error,
      true
    )
    return null
  }
  return dataUser.userPrincipalName
}

const getDataUser = async function (context, doc, upnOrId) {
  secretValues = secretValues || await getAllSecretsValues()
  let settings = setSecretsByTenant(doc.tenantFrom, secretValues)

  let dataUser = await getToken(doc.tenantFrom, settings).then(token => {
    let options = getOptionsUser(token, upnOrId)
    return getUser(options)
  })

  dataUser = JSON.parse(dataUser)

  if (dataUser && dataUser.hasOwnProperty('error')) {
    errorReprocess(
      doc._id,
      dataUser.error,
      true
    )
    return null
  }
  return dataUser
}

const updateUser = async function (context, doc, currentDataUser, dataUser) {
  secretValues = secretValues || await getAllSecretsValues()
  let settings = setSecretsByTenant(doc.tenantTo, secretValues)
  const token = await getToken(doc.tenantTo, settings).then(token => token)

  const resultInvite = await inviteUser(getOptionsToInvite(token, dataUser))

  if (resultInvite) {
    doc.body.headers.Authorization = token
    doc.body.body = currentDataUser

    const arr = doc.body.url.split('/')
    const userId = arr.slice(arr.length - 1, arr.length)

    doc.body.url = doc.body.url.replace(userId[0], resultInvite.invitedUser.id)

    let resultPatch = await patchUser(doc.body)

    if (!resultPatch) {
      reprocessed(doc._id)
      console.log(`Document: ${doc._id} reprocessed !`)
    } else {
      if (typeof resultPatch === 'string') {
        resultPatch = JSON.parse(resultPatch)
      }

      if (resultPatch.hasOwnProperty('error')) {
        console.log(`Fail reprocessed Document: ${doc._id}! ${resultPatch.error.message}`)
        errorReprocess(
          doc._id,
          resultPatch.error,
          false
        )
      }

    }

  } else {
    console.log('Resultado de Invite: ', resultInvite)
  }
}

const getOptionsUser = (token, upnOrId) => {
  return {
    method: 'GET',
    url: 'https://graph.microsoft.com/beta/users/' + upnOrId,
    headers:
      {
        'Authorization': token
      }
  }
}

const toBodyPatch = datauser => {
  return {
    givenName: datauser.givenName,
    surname: datauser.surname,
    city: datauser.city,
    country: datauser.country,
    department: datauser.department,
    displayName: datauser.displayName,
    jobTitle: datauser.jobTitle,
    mailNickname: datauser.mailNickname,
    mobilePhone: datauser.mobilePhone,
    officeLocation: datauser.mobilePhone,
    // onPremisesImmutableId: datauser.onPremisesImmutableId,
    postalCode: datauser.postalCode,
    preferredLanguage: datauser.preferredLanguage,
    streetAddress: datauser.streetAddress,
    usageLocation: datauser.usageLocation,
    showInAddressList: datauser.showInAddressList === false ? false : true,
    onPremisesExtensionAttributes: {
      extensionAttribute12: datauser.onPremisesExtensionAttributes.extensionAttribute12
    }
  }
}

const getOptionsToInvite = (token, dataUser) => {
  return {
    method: 'POST',
    url: 'https://graph.microsoft.com/beta/invitations',
    headers:
      {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
    body: {
      invitedUserDisplayName: dataUser.displayName || (`${dataUser.givenName} ${dataUser.surname}`),
      invitedUserEmailAddress: dataUser.userPrincipalName,
      invitedUserMessageInfo: {
        customizedMessageBody: 'Invitacion a Tennant',
        messageLanguage: 'string'
      },
      sendInvitationMessage: false,
      inviteRedirectUrl: 'https://invitations.microsoft.com/redeem',
      inviteRedeemUrl: 'https://visionsoftware.com'
    },
    json: true
  }
}