require('./../db/mongoose')
const ObjectId = require('mongoose').Types.ObjectId
const reproSchema = require('./../db/schema/reprocess')
const async = require('asyncawait/async')
const await = require('asyncawait/await')

const queryPatchUserReprocess = [
  {
    $match: {
      'errorMessage': {$regex: '.*connect ETIMEDOUT.*'},
      'reprocessed': false,
      'attemptEvent': {'$ne': 'inviteUser'}
    }
  },
  {$sort: {'created_at': -1}},
  {$limit: 20}
]

const queryInviteUserReprocess = [
  {
    $match: {
      'errorMessage': {$regex: '.*connect ETIMEDOUT.*'},
      'reprocessed': false,
      'attemptEvent': {'$ne': 'patchUser'}
    }
  },
  {$sort: {'created_at': -1}},
  {$limit: 20}
]

// Reprocesar error onPremisesImmutableId
// y aun no han sido reprocesados $errorReprocessing.length = 0
const queryOnPremisesImmutableIdReprocess = [
  {
    $match: {
      'reprocessed': false,
      'errorMessage': {$regex: '.*onPremisesImmutableId.*'},
    }
  },
  {
    $sort: {
      created_at: -1
    }
  },
  {
    $limit: 20
  }
]
//   [
//   {
//     $match: {
//       'errorMessage': {$regex: '.*onPremisesImmutableId.*'},
//       'reprocessed': false
//     }
//   },
//   {
//     $project: {
//       size_errorReprocessing: {$size: '$errorReprocessing'},
//       functionStack: 1,
//       attemptEvent: 1,
//       errorMessage: 1,
//       tenantFrom: 1,
//       tenantTo: 1,
//       body: 1,
//       comments: 1,
//       created_at: 1,
//       updatedAt: 1,
//       reprocessed: 1,
//       errorReprocessing: 1,
//       userNativeId: 1
//     }
//   },
//   {
//     $match: {
//       size_errorReprocessing: {$lt: 1}
//     }
//   },
//
//   {$limit: 30}
// ]

const queryFindReprocess = (userNativeId, tenantFrom, tenantTo, attemptEvent) => {

  return {
    userNativeId,
    tenantFrom,
    tenantTo,
    attemptEvent
  }
}

const testQuery = [
  {
    $match: {
      _id: {
        $in: [ObjectId('5d1e004210042b16f8ae5577'),
          ObjectId('5d1e004210042b16f8ae5576'),
          ObjectId('5d1e004210042b16f8ae5578'),
          ObjectId('5d1e004110042b16f8ae5575'),
          ObjectId('5d1ccb17ef981a079c80fa2d')
        ]
      }
    }
  }
]

const reprocess = {

  existsReprocess (userNativeId, tenantFrom, tenantTo, attemptEvent) {
    const getOne = async(function (userNativeId, tenantFrom, tenantTo, attemptEvent) {
      return await(reproSchema.findOne(queryFindReprocess(userNativeId, tenantFrom, tenantTo, attemptEvent)))
    })

    return getOne(userNativeId, tenantFrom, tenantTo, attemptEvent)
  },

  register (data) {
    const repro = new reproSchema(data)
    repro.save().then(() => console.log(repro)).catch((error => console.log(error)))
  },

  getonPremisesImmutableIdError () {

    const getData = async(function () {
      const promi = reproSchema.aggregate(queryOnPremisesImmutableIdReprocess).exec()

      return await(promi.then(colls => colls))
    })

    return getData().then(coll => coll)
  },

  getTimeOutPatchUser () {

    const getData = async(function () {
      const promi = reproSchema.aggregate(queryPatchUserReprocess).exec()

      return await(promi.then(colls => colls))
    })

    return getData().then(coll => coll)
  },

  getTimeOutInviteUser () {
    const getData = async(function () {
      const promi = reproSchema.aggregate(queryInviteUserReprocess).exec()

      return await(promi.then(colls => colls))
    })

    return getData().then(coll => coll)
  },

  errorReprocess (docId, errorReprocessing, reprocessed) {
    reproSchema.findOne({'_id': ObjectId(docId)}).exec((error, doc) => {
      doc.errorReprocessing.push(errorReprocessing)
      doc.reprocessed = reprocessed
      doc.save()
    })
  },

  reprocessed (docId) {
    reproSchema.findOne({'_id': ObjectId(docId)}).exec((error, doc) => {
      doc.reprocessed = true
      doc.save()
    })
  }
}

module.exports = reprocess