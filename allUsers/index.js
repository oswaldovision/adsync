const {getToken, getUsers} = require('./../graphHelper')
const {getAllSecretsValues, setSecretsByTenant} = require('../secretsHelper')

module.exports = async function (context, req) {

  let allValues = await getAllSecretsValues()

  let tenantId = allValues.tenants.indexOf(req.query.tenant) > 0 ? req.query.tenant : allValues.tenants[0]

  let settings = setSecretsByTenant(tenantId, allValues)
  let result = await getToken(tenantId, settings).then(token => {
    let options = {
      method: 'GET',
      url: 'https://graph.microsoft.com/v1.0/users',
      headers:
        {
          'Authorization': token
        }
    }
    return getUsers(options)
  }).catch(error => {
    context.res = {
      body: error
    }
  })

  context.res = {
    body: result,
    headers: {
      'Content-Type': 'application/json'
    }
  }

}
