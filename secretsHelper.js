const msRestAzure = require('ms-rest-azure')
const KeyVault = require('azure-keyvault')
const async = require('asyncawait/async')
const await = require('asyncawait/await')


let getAllSecretsFake = function () {
  let grantType = 'client_credentials'
  let scope = 'https://graph.microsoft.com/.default'
  let tenants = '10a76712-94f6-46a2-9155-31bd8b76f937,616e0c03-b820-4f30-9aa4-b4fdaffca42a,5506b488-3e97-4396-986b-8d2e19d98977,daa4d0ab-0563-4108-9916-441c45c84ae9,eb61b835-3227-4a54-b060-d40e362424ba,07460d2e-841a-464f-b35c-e2d761ae093a,8b56d1c5-0645-4193-8924-d3ad96922fc6,a7864c9c-6c2a-48ba-890a-9b05d542bfe0,8bb9046a-a480-4509-8228-211a8600323c,4e9a810e-6055-4e88-b023-af50fe48e528,b286c801-5b10-41fc-9526-24766ef42997,5edd4637-cbc2-4174-a177-3261e9159420,51b14b4d-f1d2-47bc-b33b-d3ff3f29b08b'
  let tenantSecret = `{"10a76712-94f6-46a2-9155-31bd8b76f937":{"clientId":"8187939a-24f3-47b8-becf-888ebdca049f","clientSecret":"IUJjZeoYf+x8Pfl5nRliDwUpE1kqFLOO1gqHKXOsHjo="},"616e0c03-b820-4f30-9aa4-b4fdaffca42a":{"clientId":"f08bf065-4f17-4a19-9827-b849bd5e1b4f","clientSecret":"LgotemceDLgUIKWd3mjqZNzhQjcpIGFUplG9Cb/VVyg="},"5506b488-3e97-4396-986b-8d2e19d98977":{"clientId":"009082b2-5003-4111-8a5f-9ee72db3fa97","clientSecret":"4Uvkkh/5zRMs0yP4ElyFYgiK1icT3dvOv1DYd4ilCGI="},"daa4d0ab-0563-4108-9916-441c45c84ae9":{"clientId":"70033fb4-f91a-46f1-84c3-89264ec9aee8","clientSecret":"9F[BQfO12P]IN8%UE0YV)Mo(oj-t[#x$"},"eb61b835-3227-4a54-b060-d40e362424ba":{"clientId":"810190b8-edd7-4c30-b663-22f0a8597ac9","clientSecret":"q]cPo)T}dY%:T>=1>OI$+Y/&]&-)B@P^"},"07460d2e-841a-464f-b35c-e2d761ae093a":{"clientId":"ffd35a14-0512-48eb-b66a-f9fd70dd6480","clientSecret":"O?78zyP-KXg/M96sQCsd@XfKJapxDLI0"},"8b56d1c5-0645-4193-8924-d3ad96922fc6":{"clientId":"d30aa317-33cf-4e83-8ad9-693bf45af270","clientSecret":".m3mPP.6hybL7s~-XAw8fa-Be_r4Iq~Sm-"},"a7864c9c-6c2a-48ba-890a-9b05d542bfe0":{"clientId":"7c37876c-41dd-4d22-888e-b8a0df0a8cda","clientSecret":"-qr28d8M06iCQE7w-UZj6eplfB~M~A1_5J"},"8bb9046a-a480-4509-8228-211a8600323c":{"clientId":"8266fc79-550b-442f-a1dd-3900b5b4bd2b","clientSecret":"-7EXR50Hld7C~TE--~Et.r4_C4MjKI-92G"},"4e9a810e-6055-4e88-b023-af50fe48e528":{"clientId":"f50a084f-cd27-450a-b104-375c3863be36","clientSecret":"_4t~SOc6Sg-3KnYNtE1qG4.v_C621BIFTm"},"b286c801-5b10-41fc-9526-24766ef42997":{"clientId":"378ef71f-2a96-41fd-aba2-288bd8eff836","clientSecret":"F8p3bw0hZ.9ok15~Ps1j-63Jxyqng3_W_W"},"5edd4637-cbc2-4174-a177-3261e9159420":{"clientId":"895bc042-c56a-40a5-93bd-3a1d9ac9e190","clientSecret":"EAlkz_1mir6.bw8_0a5M4BVKF_w~9npWWC"},"51b14b4d-f1d2-47bc-b33b-d3ff3f29b08b":{"clientId":"764f97c4-4c2b-406f-8d88-d39d8c5093e5","clientSecret":"yKqz_e9F8.~0fwCH99MqDO2GOWtVgv-34N"}}`

  return {
    tenantSecret: JSON.parse(tenantSecret),
    grantType,
    scope,
    tenants: tenants.split(',')
  }
}

let getAllSecrets = async(function () {
  let credentials = await(secretHelper.getKeyVaultCredentials())
  let grantType = await(secretHelper.getKeyVaultSecret(credentials, 'grantType'))
  let scope = await(secretHelper.getKeyVaultSecret(credentials, 'scope'))
  let tenants = await(secretHelper.getKeyVaultSecret(credentials, 'tenants'))
  let tenantSecret = await(secretHelper.getKeyVaultSecret(credentials, 'tenantSecret'))

  return {
    tenantSecret: JSON.parse(tenantSecret.value),
    grantType: grantType.value,
    scope: scope.value,
    tenants: tenants.value.split(',')
  }
})

let dbConnectionSettings = async(function () {
  let credentials = await(secretHelper.getKeyVaultCredentials())
  let mongoUri = await(secretHelper.getKeyVaultSecret(credentials, 'mongoUri'))
  let mongoUser = await(secretHelper.getKeyVaultSecret(credentials, 'mongoUser'))
  let mongoPassword = await(secretHelper.getKeyVaultSecret(credentials, 'mongoPassword'))

  return {
    mongoUri: mongoUri.value,
    mongoUser: mongoUser.value,
    mongoPassword: mongoPassword.value
  }
})

const secretHelper = {
  getKeyVaultCredentials: () => {
    return msRestAzure.loginWithAppServiceMSI()
  },

  getKeyVaultSecret: (credentials, key) => {
    credentials.resource = process.env.RESOURCE
    let keyVaultClient = new KeyVault.KeyVaultClient(credentials)
    return keyVaultClient.getSecret(process.env.KEY_VAULT_URI, key, '')
  },

  getAllSecretsValues: () => {
    return getAllSecrets()
  },

  getSettingsDb: () => {
    return dbConnectionSettings()
  },

  setSecretsByTenant: (currentTenant, allSecrets) => {
    allSecrets.clientId = allSecrets.tenantSecret[currentTenant].clientId
    allSecrets.clientSecret = allSecrets.tenantSecret[currentTenant].clientSecret

    return allSecrets
  }
}

module.exports = secretHelper
