const {getTimeOutPatchUser, getTimeOutInviteUser, reprocessed, errorReprocess} = require('./../process/reprocess')
const {getUser, getToken, patchUser, inviteUser} = require('./../graphHelper')
const {getAllSecretsValues, setSecretsByTenant} = require('./../secretsHelper')

let secretValues

const reprocessPatchUsers = async (context, docs) => {
  for (let i = 0; i < docs.length; i++) {

    let doc = docs[i]

    let data

    if (doc.userNativeId) {
      data = await getDataUser(context, doc, doc.userNativeId)
    } else {
      const arr = doc.body.url.split('/')
      const userId = arr.slice(arr.length - 1, arr.length)
      const upn = await getUpn(context, doc, userId[0])
      if (upn) {
        data = await getDataUser(context, doc, upn.split('#EXT#')[0].replace('_', '@'))
      }
    }

    if (data) {
      await updateUser(context, doc, toBodyPatch(data))
    }
  }
}

const reprocessInviteUsers = async (context, docs) => {
  for (let i = 0; i < docs.length; i++) {
    const doc = docs[i]
    await inviteReprocess(context, doc)
  }
}

module.exports = async function (context, myTimer) {
  const docsPatchUserReprocess = await getTimeOutPatchUser()
  const docInviteUserReprocess = await getTimeOutInviteUser()

  await reprocessPatchUsers(context, docsPatchUserReprocess)
  await reprocessInviteUsers(context, docInviteUserReprocess)
}

const getUpn = async function (context, doc, userId) {
  secretValues = secretValues || await getAllSecretsValues()
  let settings = setSecretsByTenant(doc.tenantTo, secretValues)

  let dataUser = await getToken(doc.tenantTo, settings).then(token => {
    let options = getOptionsUser(token, userId)
    return getUser(options)
  })

  dataUser = JSON.parse(dataUser)

  if (dataUser && dataUser.hasOwnProperty('error')) {
    errorReprocess(
      doc._id,
      dataUser.error,
      true
    )
    return null
  }
  return dataUser.userPrincipalName
}

const getDataUser = async function (context, doc, upnOrId) {
  secretValues = secretValues || await getAllSecretsValues()
  let settings = setSecretsByTenant(doc.tenantFrom, secretValues)

  let dataUser = await getToken(doc.tenantFrom, settings).then(token => {
    let options = getOptionsUser(token, upnOrId)
    return getUser(options)
  })

  dataUser = JSON.parse(dataUser)

  if (dataUser && dataUser.hasOwnProperty('error')) {
    errorReprocess(
      doc._id,
      dataUser.error,
      true
    )
    return null
  }
  return dataUser
}

const inviteReprocess = async (context, doc) => {
  secretValues = secretValues || await getAllSecretsValues()
  let settings = setSecretsByTenant(doc.tenantTo, secretValues)

  let result = await getToken(doc.tenantTo, settings).then(token => {
    doc.body.headers.Authorization = token
    return inviteUser(doc.body)
  })

  if (result && result.hasOwnProperty('error')) {
    errorReprocess(
      doc._id,
      result.error,
      false
    )
  } else {
    reprocessed(doc._id)
  }
}

const updateUser = async function (context, doc, currentDataUser) {
  secretValues = secretValues || await getAllSecretsValues()
  let settings = setSecretsByTenant(doc.tenantTo, secretValues)

  await getToken(doc.tenantTo, settings).then(token => {
    doc.body.headers.Authorization = token
    doc.body.body = currentDataUser

    patchUser(doc.body)
      .then(result => {
        if (result && result.hasOwnProperty('error')) {
          errorReprocess(
            doc._id,
            result.error,
            true
          )
        } else if (!result) {
          reprocessed(doc._id)
        }
      })
  })
}

const getOptionsUser = (token, upnOrId) => {
  return {
    method: 'GET',
    url: 'https://graph.microsoft.com/beta/users/' + upnOrId,
    headers:
      {
        'Authorization': token
      }
  }
}

const toBodyPatch = datauser => {
  return {
    givenName: datauser.givenName,
    surname: datauser.surname,
    city: datauser.city,
    country: datauser.country,
    department: datauser.department,
    displayName: datauser.displayName,
    jobTitle: datauser.jobTitle,
    mailNickname: datauser.mailNickname,
    mobilePhone: datauser.mobilePhone,
    officeLocation: datauser.mobilePhone,
    onPremisesImmutableId: datauser.onPremisesImmutableId,
    postalCode: datauser.postalCode,
    preferredLanguage: datauser.preferredLanguage,
    // state: datauser.state,
    streetAddress: datauser.streetAddress,
    usageLocation: datauser.usageLocation,
    showInAddressList: datauser.showInAddressList === false ? false : true,
    onPremisesExtensionAttributes: {
      extensionAttribute12: datauser.onPremisesExtensionAttributes.extensionAttribute12
    }
  }
}