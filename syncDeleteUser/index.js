const {getToken, getUser, deleteUser} = require('./../graphHelper')
const {getAllSecretsValues, setSecretsByTenant} = require('../secretsHelper')

module.exports = async function (context, req) {
  context.log('JavaScript HTTP trigger function processed a request.')

  if (!req.body.tenant || !req.body.upn) {
    context.res = {
      status: 500,
      body: 'El id tenant y el upn son requeridos !'
    }
    return
  }

  let tenantId = req.body.tenant
  let upnToDelete = req.body.upn

  //get tenants from vault
  let allValues = await getAllSecretsValues()

  context.log(allValues.tenants.join(','))

  let tenants = allValues.tenants

  let setTenants = tenants.filter(v => v != tenantId)

  let success = []

  context.log('los tenant restantes: ', setTenants.join(','))

  for (let i = 0; i < setTenants.length; i++) {
    context.log('UPN: ', upnToDelete)

    // obtener el usuario para usar su mail en el delete
    let settings = setSecretsByTenant(setTenants[i], allValues)
    let userToDelete = await getToken(setTenants[i], settings).then(token => {
      context.log('EL TOKEN: ', token)
      let options = {
        method: 'GET',
        url: `https://graph.microsoft.com/v1.0/users?$filter=mail eq '${upnToDelete}'`,
        headers:
          {
            'Authorization': token
          }
      }
      return getUser(options)
    }).catch(error => {
      context.log('Error obteniendo usuario : ', error)
    })

    context.log('El USUARIO: ', userToDelete)

    let objUser = JSON.parse(userToDelete)

    // Delete usuario
    let configValues = setSecretsByTenant(setTenants[i], allValues)
    await getToken(setTenants[i], configValues).then(token => {

      let options = {
        method: 'DELETE',
        url: 'https://graph.microsoft.com/beta/users/' + objUser.value[0].id,
        headers:
          {
            'Authorization': token,
            'Content-Type': 'application/json'
          }
      }
      return deleteUser(options)
    }).then(result => {
      success.push(true)
    }).catch(error => {
      success.push(false)
      context.log('ERROR: ', error)
    })
  }

  if (success.every(value => {return value === true})) {
    context.res = {
      headers: {
        'Content-Type': 'application/json'
      },
      body: {'Sync tenants: Delete users in tenants: ': setTenants.join(' ')}
    }
  } else {
    context.res = {
      status: 500,
      body: 'Error Delete user !'
    }
  }
}
