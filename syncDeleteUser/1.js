const request = require('request')

const getOptionsInvite = function (displayName, email, tenant, idUser) {
  return {
    method: 'POST',
    url: `https://aadsynccore.azurewebsites.net/api/syncNewUser?code=H3Uw1tlMvFOEHcCTrKiCFMGiY4t6bfM6fQmCRHOm1Awg4emssZldgQ==&tenant=${tenant}`,
    headers:
      {
        'Content-Type': 'application/json'
      },
    body: {
      id: idUser,
      invitedUserDisplayName: displayName,
      invitedUserEmailAddress: email,
      invitedUserMessageInfo: {
        customizedMessageBody: 'Invitacion a Tennant',
        messageLanguage: 'string'
      },
      sendInvitationMessage: false,
      inviteRedirectUrl: 'https://invitations.microsoft.com/redeem',
      inviteRedeemUrl: 'https://visionsoftware.com'
    },
    json: true
  }
}

const getOptionsUpdate = function (tenant, id) {
  return {
    method: 'POST',
    url: 'https://aadsynccore.azurewebsites.net/api/syncUser?code=620WZPt/1HqaM0MFEI04BvajdvqvSozZl9LCDjazZdJJ0X0lhCeJ6Q==',
    headers:
      {
        'Content-Type': 'application/json'
      },
    body: {
      id,
      tenant
    },
    json: true
  }
}

const getOptionsDelete = function (tenant, upn) {
  return {
    method: 'POST',
    url: 'https://aadsynccore.azurewebsites.net/api/syncDeleteUser?code=Jvtr0wHZBzvz3UToBnOcaXQaemCS/09eLwk5xUEtnwzYq05eGO1ooA==',
    headers:
      {
        'Content-Type': 'application/json'
      },
    body:
      {
        tenant: tenant,
        upn: upn
      },
    json: true
  }
}

module.exports = function (context, eventHubMessages) {
  let allowedValues = ['Add user', 'Update user', 'Delete user']
  context.log(`JavaScript eventhub trigger function called for message array: ${eventHubMessages}`)

  eventHubMessages.forEach(function (message) {
    context.log(`Processed message: ${JSON.stringify(message)}`)
    let newProcess = message

    if (typeof newProcess.records != 'object') {
      context.log(`input is an error process log, process only success process ! ${newProcess}`)
      return
    }

    newProcess.records.forEach(function (objLog) {
      if (objLog.resultType == 'Success' && allowedValues.includes(objLog.operationName)) {
        let tenant = objLog.tenantId
        let email = objLog.properties.targetResources[0].userPrincipalName
        let idUser = objLog.properties.targetResources[0].id
        let displayName = objLog.properties.targetResources[0].userPrincipalName.split('@')[0]

        context.log('PROPS : ', tenant, email, displayName)

        if (displayName.includes('EXT')) {
          context.log(`EXT users no allowed: ${displayName} !`)
          return
        }

        let settingsRequest = {}

        switch (objLog.operationName) {
          case 'Add user':
            settingsRequest = getOptionsInvite(displayName, email, tenant, idUser)
            break
          case 'Update user':
            settingsRequest = getOptionsUpdate(tenant, idUser)
            break
          case 'Delete user':
            let clearId = idUser.replace(/-/g, '')
            let clearUpn = email.replace(clearId, '')
            settingsRequest = getOptionsDelete(tenant, clearUpn)
            break
          default:
            context.log(`${objLog.operationName} Only sync types: Add,Update and Delete users`)
        }

        request(settingsRequest, function (error, response, body) {
          if (error) {
            context.log(`ERROR REQUEST: ${error.message}`)
            return
          } else {
            context.log(`REQUEST OK, result: ${body}`)
            return
          }
        })

      } else {
        context.log(`Excluded event: ${objLog.operationName}: Only sync types: Add,Update and Delete users OR ${objLog.resultType}: No allowed error events`)
        return
      }
    })

  })
  context.done()
}
