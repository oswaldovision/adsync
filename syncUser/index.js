const {getToken, patchUser, getUser, inviteUser} = require('./../graphHelper')
const {getAllSecretsValues, setSecretsByTenant} = require('../secretsHelper')
const {register, existsReprocess} = require('./../process/reprocess')

const toBodyPatch = datauser => {
  return {
    givenName: datauser.givenName,
    surname: datauser.surname,
    city: datauser.city,
    country: datauser.country,
    department: datauser.department,
    displayName: datauser.displayName,
    jobTitle: datauser.jobTitle,
    mailNickname: datauser.mailNickname,
    mobilePhone: datauser.mobilePhone,
    officeLocation: datauser.mobilePhone,
    onPremisesImmutableId: datauser.onPremisesImmutableId,
    postalCode: datauser.postalCode,
    preferredLanguage: datauser.preferredLanguage,
    // state: datauser.state,
    streetAddress: datauser.streetAddress,
    usageLocation: datauser.usageLocation,
    // userPrincipalName: datauser.userPrincipalName,
    // userType: datauser.userType,
    showInAddressList: datauser.showInAddressList === false ? false : true,
    onPremisesExtensionAttributes: {
      extensionAttribute12: datauser.onPremisesExtensionAttributes.extensionAttribute12,
      extensionAttribute13: datauser.onPremisesExtensionAttributes.extensionAttribute13,
      extensionAttribute14: datauser.onPremisesExtensionAttributes.extensionAttribute14

    }
  }
}

const invitationToReprocess = (userNativeId, errorMessage, tenantFrom, tenantTo, optionsToInvite, comments) => {
  const data = {
    functionStack: 'syncUser',
    userNativeId,
    attemptEvent: 'inviteUser',
    errorMessage,
    tenantFrom,
    tenantTo,
    body: optionsToInvite,
    comments,
  }
  register(data)
}

const patchToReprocess = (userNativeId, errorMessage, tenantFrom, tenantTo, optionsToInvite, comments) => {
  const data = {
    functionStack: 'syncUser',
    userNativeId,
    attemptEvent: 'patchUser',
    errorMessage,
    tenantFrom,
    tenantTo,
    body: optionsToInvite,
    comments,
  }
  register(data)
}

module.exports = async function (context, req) {
  //get tenants from vault
  let allValues = await getAllSecretsValues()

  context.log(allValues.tenants.join(','))

  let tenants = allValues.tenants

  if (!req.body.tenant || !req.body.id) {
    context.res = {
      status: 500,
      body: 'El id tenant y el id de usuario son requeridos !'
    }
    return
  }

  let tenantId = req.body.tenant
  let id = req.body.id

  let settings = setSecretsByTenant(tenantId, allValues)

  let result = await getToken(tenantId, settings).then(token => {
    let options = {
      method: 'GET',
      url: 'https://graph.microsoft.com/beta/users/' + id,
      headers:
        {
          'Authorization': token
        }
    }
    return getUser(options)
  })

  let dataUser = JSON.parse(result)

  let setTenants = tenants.filter(v => v != tenantId)

  context.log(`Native user: ${dataUser.userPrincipalName}
  data to update: ${JSON.stringify(toBodyPatch(dataUser))}`)

  for (let i = 0; i < setTenants.length; i++) {

    // obtener el usuario para usar su mail en el patch
    let configValues = setSecretsByTenant(setTenants[i], allValues)
    let token = await getToken(setTenants[i], configValues).then(resultToken => {
      return resultToken
    })

    let upnReference = dataUser.userPrincipalName.replace(/@/, '_')
    let options = {
      method: 'GET',
      url: `https://graph.microsoft.com/beta/users?$filter=startswith(userPrincipalName,'${upnReference}')`,
      headers:
        {
          'Authorization': token
        }
    }
    let userToUpdate = await getUser(options).then(user => {
      return user
    })

    let objUser = JSON.parse(userToUpdate)
    let idUser = ''
    // Si el usuario no existe en el current tenant
    if (objUser.error || !objUser.value.length) {
      context.log(`Guest user ${dataUser.userPrincipalName} NOT found on this tenant --> ${setTenants[i]}, ...starting process to invite with: ${dataUser.userPrincipalName}`)
      //new invite
      let optionsToInvite = {
        method: 'POST',
        url: 'https://graph.microsoft.com/beta/invitations',
        headers:
          {
            'Authorization': token,
            'Content-Type': 'application/json'
          },
        body: {
          invitedUserDisplayName: dataUser.displayName || (`${dataUser.givenName} ${dataUser.surname}`),
          invitedUserEmailAddress: dataUser.userPrincipalName,
          invitedUserMessageInfo: {
            customizedMessageBody: 'Invitacion a Tennant',
            messageLanguage: 'string'
          },
          sendInvitationMessage: false,
          inviteRedirectUrl: 'https://invitations.microsoft.com/redeem',
          inviteRedeemUrl: 'https://visionsoftware.com'
        },
        json: true
      }

      let newUser = await inviteUser(optionsToInvite).then(result => {
        if (result && result.hasOwnProperty('error')) {
          const message = JSON.stringify(result.error)
          context.log(`Invitation failed ! ${message}`)

          //check if there is previous reprocess
          existsReprocess(dataUser.id, tenantId, setTenants[i], 'inviteUser')
            .then(existsDoc => {
              if (!existsDoc) {
                //Registering Reprocess
                invitationToReprocess(dataUser.id, message, tenantId, setTenants[i], optionsToInvite, `Invitation failed: result error`)
              }
            })

          return null
        } else {
          context.log(`Invitation success to user ${dataUser.userPrincipalName}: data: ${JSON.stringify(result)}`)
          return result
        }
      }).catch((error) => {
        context.log(`Invitation failed ! on tenant: ${error.message}`)

        //check if there is previous reprocess
        existsReprocess(dataUser.id, tenantId, setTenants[i], 'inviteUser')
          .then(existsDoc => {
            if (!existsDoc) {
              //Registering Reprocess
              invitationToReprocess(dataUser.id, error.message, tenantId, setTenants[i], optionsToInvite, `Invitation failed: catch error`)
            }
          })

        return null
      })

      idUser = newUser
    } else {
      idUser = {
        'invitedUser': {
          'id': objUser.value[0].id
        }
      }

      context.log(`Guest user ${objUser.value[0].userPrincipalName} found in this tenant --> ${setTenants[i]} 
      Old data: ${JSON.stringify(objUser.value[0])}`)
    }

    if (idUser) {
      // patch Usuario
      let optionsToPatch = {
        method: 'PATCH',
        url: 'https://graph.microsoft.com/beta/users/' + idUser.invitedUser.id,
        headers:
          {
            'Authorization': token,
            'Content-Type': 'application/json'
          },
        body: toBodyPatch(dataUser),
        json: true
      }

      await patchUser(optionsToPatch)
        .then(result => {
          if (result && result.hasOwnProperty('error')) {
            const message = JSON.stringify(result.error)
            context.log(`Patch failed ! ${message}`)

            //check if there is previous reprocess
            existsReprocess(dataUser.id, tenantId, setTenants[i], 'patchUser')
              .then(existsDoc => {
                if (!existsDoc) {
                  //Registering Reprocess
                  patchToReprocess(dataUser.id, message, tenantId, setTenants[i], optionsToPatch, `Patch user failed: result error`)
                }
              })

          }
        })
        .catch(error => {
          context.log('ERROR: ', error)

          //check if there is previous reprocess
          existsReprocess(dataUser.id, tenantId, setTenants[i], 'patchUser')
            .then(existsDoc => {
              if (!existsDoc) {
                //Registering Reprocess
                patchToReprocess(dataUser.id, error.message, tenantId, setTenants[i], optionsToPatch, `Patch user failed: catch error`)
              }
            })

          context.res = {
            body: error
          }
        })
    }
  }

  context.res = {
    status: 200,
    body: 'Sync tenants: ' + setTenants.join(' ')
  }

}
