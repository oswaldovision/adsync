const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  functionStack: String,
  userNativeId: String,
  attemptEvent: String,
  errorMessage: String,
  tenantFrom: String,
  tenantTo: String,
  errorReprocessing: [{}],
  body: {},
  reprocessed: {
    type: Boolean,
    default: false
  },
  comments: String
}, {
  timestamps:
    {
      createdAt: 'created_at'
    }
})

module.exports = mongoose.model('Reprocess', schema)