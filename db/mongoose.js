const {connect} = require('mongoose')
const {getSettingsDb} = require('./../secretsHelper')
const async = require('asyncawait/async')
const await = require('asyncawait/await')

const getSettings = async(function () {
  const settings = await(getSettingsDb().then((values) => { return values}))
  return settings
})

getSettings().then(settingsDb => {
  console.log(settingsDb)
  connect(settingsDb.mongoUri, {
    useNewUrlParser: true,
    auth: {
      user: settingsDb.mongoUser,
      password: settingsDb.mongoPassword
    }
  })
    .then(() => console.log('Connection to CosmosDB successful'))
    .catch((err) => console.error(err))
})
  .catch((err) => console.error('ERROR:' + err))