const {getToken, patchUser} = require('./../graphHelper')
const {getAllSecretsValues, setSecretsByTenant} = require('../secretsHelper')

module.exports = async function (context, req) {

  let allValues = await getAllSecretsValues()

  let tennantId = allValues.tenants.indexOf(req.query.tenant) > 0 ? req.query.tenant : allValues.tenants[0]

  let settings = setSecretsByTenant(tennantId, allValues)

  let result = await getToken(tennantId, settings).then(token => {

    let options = {
      method: 'PATCH',
      url: 'https://graph.microsoft.com/beta/users/' + req.query.id,
      headers:
        {
          'Authorization': token,
          'Content-Type': 'application/json'
        },
      body: req.body,
      json: true
    }
    return patchUser(options)
  }).catch(error => {
    context.res = {
      body: error
    }
  })

  context.res = {
    status: 200,
    body: req.body,
    headers: {
      'Content-Type': 'application/json'
    }
  }

}
