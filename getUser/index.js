const {getToken, getUser} = require('./../graphHelper')
const {getAllSecretsValues, setSecretsByTenant} = require('../secretsHelper')

module.exports = async function (context, req) {

  let allValues = await getAllSecretsValues()

  let tennantId = allValues.tenants.indexOf(req.query.tenant) > 0 ? req.query.tenant : allValues.tenants[0]

  let settings = setSecretsByTenant(tennantId, allValues)
  let result = await getToken(tennantId, settings).then(token => {
    let options = {
      method: 'GET',
      url: 'https://graph.microsoft.com/beta/users/' + req.query.id,
      headers:
        {
          'Authorization': token
        }
    }
    return getUser(options)
  }).catch(error => {
    context.res = {
      body: error
    }
  })

  context.res = {
    body: result,
    headers: {
      'Content-Type': 'application/json'
    }
  }

}
